# kickstarter
##Proyecto de DataScience
DS_final_project_EDA+Alg_scores.ipynb  -> Análisis Exploratorio de datos(EDA) + Prueba de algunas técnicas de ML y verificación de scores
DS_final_project_modeling.ipynb  -> Machine Learning


##Poster
Poster_Data_Science.pdf
1. Título del proyecto
 Integrantes del grupo
2. Resumen
3. Introducción y justificación
4. Objetivos
5. Metodología
6. Resultados
7. Conclusión
8. Referencias bibliográficas

##Dataset
ks-projects-201801.csv


##Archivo completo comprimido
TP - DS - kickstarter.zip
